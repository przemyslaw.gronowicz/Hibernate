/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.hibernate.main;

import java.util.List;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.model.Customer;
import pl.sda.inheritance.model.FourWheeler;
import pl.sda.inheritance.model.TwoWheeler;
import pl.sda.inheritance.model.Vehicle;

/**
 *
 * @author PsLgComp
 */
public class Main {
    
    public static void main(String[] args){
       
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
//        
//        Session session = ConfigHibernate.getInstance().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();   
            Vehicle vehicle = new Vehicle("New vehicle17");
            FourWheeler fw = new FourWheeler("New stering type","FourWheeler17");
            TwoWheeler tw = new TwoWheeler("Two wh. steering","BrandNew TwoWheeler17");
            session.save(vehicle);  
            session.save(fw);
            session.save(tw);
            tx.commit();
            
//            tx = session.beginTransaction();
//            
//   
//            Query query = session.createQuery("from Vehicle");
//            Optional<Vehicle> optVehicle = query.list().stream().findFirst();
//            //optVehicle.ifPresent(v -> System.out.println(v.getName()));
//            if(optVehicle.isPresent()){
//                Vehicle outV = optVehicle.get();
//                System.out.println(outV.getName());
//                //session.save(outV);
//                //session.evict(outV);
//                outV.setName("non-standard vehicle");
//                //session.evict(outV);
//            }
//            tx.commit();
            
                    }catch(HibernateException e){
            if(tx != null) tx.rollback();
            e.printStackTrace();
        }finally{
            session.close();
        }    
    
    }
    
}
