/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.inheritance.model;

import javax.persistence.*;

/**
 *
 * @author RENT
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "vehicle")
public class Vehicle extends AbsClass{
     private String name;

    public Vehicle() {
    }
     
     

    public Vehicle(String name) {
        this.name = name;
    }
     
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
