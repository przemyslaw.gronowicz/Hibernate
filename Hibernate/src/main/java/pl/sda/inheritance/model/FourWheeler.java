/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.inheritance.model;

import javax.persistence.*;

/**
 *
 * @author PsLgComp
 */

@Entity
public class FourWheeler extends Vehicle{
    @Column(name = "steering_type")
    protected String steeringType;

    public FourWheeler() {
    }
    
    

    public FourWheeler(String steeringType, String name) {
        super(name);
        this.steeringType = steeringType;
    }
    
}
