/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.inheritance.model;

import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author RENT
 */

@MappedSuperclass
abstract class AbsClass {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@GeneratedValue(strategy = GenerationType.TABLE)
    private int id;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date editDate;
   
}
