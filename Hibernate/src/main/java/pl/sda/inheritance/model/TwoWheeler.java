/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.inheritance.model;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author PsLgComp
 */
@Entity
public class TwoWheeler extends Vehicle{
    @Column(name = "steering_type")
    protected String steeringType;

    public TwoWheeler() {
    }
    
    

    public TwoWheeler(String steeringType, String name) {
        super(name);
        this.steeringType = steeringType;
    }
    
}
